<?php

    class Animal
    {
        private $legs = 4;
        private $cold_blooded = "no";
        private $name;

        public function __construct($name)
        {
            $this->name = $name;
        }

        public function getName()
        {
            return $this->name;
        }

        public function getLegs()
        {
            return $this->legs;
        }

        public function getColdBlooded()
        {
            return $this->cold_blooded;
        }
        
    }
    



?>