<?php
    require_once 'animal.php';
    require_once 'ape.php';
    require_once 'frog.php';

    
    $sheep = new Animal("shaun");
    // echo $sheep->name; // "shaun"
    // echo "<br>";
    // echo $sheep->legs; // 4
    // echo "<br>";
    // echo $sheep->cold_blooded; // "no"
    // echo "<br>";
    echo "Name : ".$sheep->getName()."<br>";
    echo "Legs : ".$sheep->getLegs()."<br>";
    echo "Cold Blooded : ".$sheep->getColdBlooded()."<br>";
    echo "<br>";

    $kodok = new Frog("buduk");
    echo "Name : ".$kodok->getName()."<br>";
    echo "Legs : ".$kodok->getLegs()."<br>";
    echo "Cold Blooded : ".$kodok->getColdBlooded()."<br>";
    echo "Jump : ".$kodok->jump()."<br>";
    echo "<br>";

    $sungokong = new Ape("kera sakti");
    echo "Name : ".$sungokong->getName()."<br>";
    echo "Legs : ".$sungokong->getLegs()."<br>";
    echo "Cold Blooded : ".$sungokong->getColdBlooded()."<br>";
    echo "Yell : ".$sungokong->yell()."<br>";

?>